import Welcome from './pages/Welcome';
import Product from './pages/Product';
//import { Route } from 'react-router';
import MainHeader from './components/MainHeader';
import ProductDetail from './pages/ProductDetail';
import { Route, Switch, Redirect } from 'react-router-dom';


// productId for mutiple routing.
// switch is helps to actiivate the one react at a time.
// exact for the particular matching..


import './App.css';

function App() {
  return (
   
    <div>
      <MainHeader />
      <main>
      <Switch>                                      
        <Route path='/' exact>
          <Redirect to = '/welcome'></Redirect> 

        </Route>
      <Route path="/welcome">       
        <Welcome />
      </Route>
      
      <Route path="/product" exact>
        <Product />
      </Route>
      <Route path="/product/:productId">
        <ProductDetail />
      </Route>
     
      </Switch>
      </main>
    </div>
    
  );
}

export default App;
